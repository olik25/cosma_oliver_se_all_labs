package Ex2Lab3Cosma;
import java.util.Scanner;
public class Circle {
    private double radius;
    private String color = "red";
    int length;
    int position;
    int otherposition;
    public Circle(int a) {
        length = a;
        position = 0;
    }
    public Circle(int a, int l) {
        length = a;
        position = l;
        otherposition = 0;
    }
    public Circle() {
    }
    int getRadius() {
        int k;
        Scanner in = new Scanner(System.in);
        k = in.nextInt();
        return k;
    }
    void getArea(int k) {
        double area = 3.14 * k * k;
        System.out.println(area);
    }
}