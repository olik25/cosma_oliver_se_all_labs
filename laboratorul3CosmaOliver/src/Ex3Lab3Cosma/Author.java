package Ex3Lab3Cosma;

import java.util.Scanner;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }


    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public String toString() {
        System.out.print("email:");
        System.out.print(name);
        System.out.print("(");
        System.out.print(gender);
        System.out.print(")@");
        System.out.print(email);
        System.out.print(".com");
        return null;
    }


}
