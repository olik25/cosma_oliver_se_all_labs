package Ex4Lab3Cosma;

public class MyPoint {
    double x;
    double y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(double var1, double var2) {
        x = var1;
        y = var2;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setXY(double var1, double var2) {
        this.x = var1;
        this.y = var2;
    }

    public String toString() {
        System.out.println("(" + x + "," + y + ")");
        return null;
    }

    public void getDistance(double a, double b) {
        System.out.println( Math.sqrt((a - x) * (a - x) + (b - y) * (b - y)));
    }
}
