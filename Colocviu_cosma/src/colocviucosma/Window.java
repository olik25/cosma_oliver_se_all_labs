package colocviucosma;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;


public class Window extends JFrame {
    //Attributes (Window components)
    Container c;
    JButton but;
    TextField textf;
    JTextArea display;
    //The window construction
    public Window(String name) {

        //The global variable which stores how many times the button was clicked
        super(name);
        init();
        //Set the Window visible on the screen and set it's Default Close Operation
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);


    }
    public void init()
    {
        ///WINDOW + CONTAINER///
        this.setBounds(300, 200, 500, 500);
        //Colors the header
        this.setBackground(Color.CYAN);
        //Colors the background
        this.c = getContentPane();
        this.c.setLayout(null);
        this.c.setBackground(Color.GREEN);
        //textarea
        display = new JTextArea();
        display.setBounds(0,0,100,100);
        display.setBackground(Color.WHITE);
        display.setForeground(Color.RED);
        display.setFont(new Font("Serif", Font.ITALIC, 16));
        display.setLineWrap(true);
        display.setWrapStyleWord(true);
        c.add(display);

        //button
        but = new JButton("Click!");
        but.setBounds(350,350,100,100);
        but.addActionListener(new ClickHandeler());
        but.setForeground(Color.BLUE);
        but.setOpaque(true);
        //put on window
        c.add(but);
    }
    class ClickHandeler implements ActionListener{

        Random rand=new Random();

        //action on button
        public void actionPerformed(ActionEvent e){
            int x=rand.nextInt(100);
            String newline = "\n";
            Window.this.display.setText(x +newline);
            display.append("\n");
        }
    }
}