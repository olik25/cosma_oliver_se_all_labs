package Ex1Lab4Cosma;

import java.util.Scanner;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle() {
    }

    public Circle(double newradius) {
        radius = newradius;
    }

    public double getRadius() {
        int r;
        Scanner in = new Scanner(System.in);
        r = in.nextInt();
        return r;
    }

    public double getArea() {

        return radius*radius*3.14;
    }
    public static void main(String[] args)
    {
        Circle c1=new Circle();
        c1.getRadius();
        System.out.println(c1.getArea());
    }
}
