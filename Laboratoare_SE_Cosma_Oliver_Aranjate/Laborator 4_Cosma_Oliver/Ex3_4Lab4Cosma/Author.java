package Ex3_4Lab4Cosma;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name1, String email1, char gender1) {
        name = name1;
        email = email1;
        gender = gender1;
    }

    public String getName() {
        System.out.println(name);
        return null;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public String toString() {
        System.out.print("email:");
        System.out.print(name);
        System.out.print("(");
        System.out.print(gender);
        System.out.print(")@");
        System.out.print(email);
        System.out.print(".com");
        return null;
    }
}