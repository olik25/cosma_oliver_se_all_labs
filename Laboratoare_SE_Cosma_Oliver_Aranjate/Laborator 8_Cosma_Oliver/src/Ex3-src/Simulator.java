package Lab8_ex03;

public class Simulator {
    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station Iasi
        Controler c3 = new Controler("Iasi");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers using the ArrayLists

        c1.addNeighbourController(c2);
        c1.addNeighbourController(c3);

        c2.addNeighbourController(c1);
        c2.addNeighbourController(c3);

        c3.addNeighbourController(c1);
        c3.addNeighbourController(c2);

        //testing

        Train t1 = new Train("Bucuresti", "IC-Cluj1");
        s1.arriveTrain(t1);


        Train t2 = new Train("Cluj-Napoca","R-Buc1");
        s5.arriveTrain(t2);

        Train t3 = new Train("Bucuresti", "IC-Cluj2");
        s2.arriveTrain(t3);

        Train t4 = new Train("Cluj-Napoca","R-Ias1");
        s7.arriveTrain(t4);

        Train t5 = new Train("Iasi","R-Clj12");
        s3.arriveTrain(t5);

        Train t6 = new Train("Iasi","R-BUC33");
        s6.arriveTrain(t6);


        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }

}
