package Lab8_ex04;

import org.w3c.dom.events.Event;

import java.util.Random;

//Temperature Sensor "connected" to Cooling and Heating System

public class Tsensor {
    //Attributes
    private CoolingSystem cools;
    private HeatingSystem heats;
    public int temperature;
    private int id;

    //Constructors
    public Tsensor(int Id, CoolingSystem c, HeatingSystem h) {
        this.id = Id;
        this.cools = c;
        this.heats = h;
    }

    public CoolingSystem getCooling() {
        return this.cools;
    }

    public HeatingSystem getHeats() {
        return this.heats;
    }


}
