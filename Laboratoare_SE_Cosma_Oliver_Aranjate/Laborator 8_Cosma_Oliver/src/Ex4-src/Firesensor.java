package Lab8_ex04;

//Fire Sensor "connected" to Alarm and Gsm Unit

public class Firesensor {
    //Attributes
    private int id;
    private GsmUnit gsm;
    private Alarm alarm;
    private Boolean smoke;

    //Constructors
    Firesensor(int id, GsmUnit g, Alarm a)
    {
        this.id=id;
        this.gsm=g;
        this.alarm=a;
    }

    //Methods

    public Alarm GetAlarms(){return this.alarm;}
    public GsmUnit GetGsm(){return this.gsm;}
    public int GetId(){return this.id;}
    public Boolean GetSmoke(){return this.smoke;}
    public void SetSmoke(Boolean b){this.smoke=b;}
}
