package Lab8_ex04;

import org.w3c.dom.events.Event;

import java.util.ArrayList;
import java.util.Random;


///singleton!!!
public class Controller {

    private static Controller ContUnit = null;

    private Tsensor TemperatureSensor;
    private ArrayList<Firesensor> fireList = new ArrayList<Firesensor>();
    private Random r = new Random();

    //Constructor with singleton
    private Controller() {
        TemperatureSensor = new Tsensor(11, new CoolingSystem(), new HeatingSystem());
        Firesensor f1 = new Firesensor(0, new GsmUnit(), new Alarm());
        Firesensor f2 = new Firesensor(1, new GsmUnit(), new Alarm());
        Firesensor f3 = new Firesensor(2, new GsmUnit(), new Alarm());
        Firesensor f4 = new Firesensor(3, new GsmUnit(), new Alarm());
        fireList.add(f1);
        fireList.add(f2);
        fireList.add(f3);
        fireList.add(f4);
    }

    public static Controller getController() {
        if (ContUnit != null) return ContUnit;
        else return new Controller();
    }

    //Checks and resolve the events
    public void control(int event) {
        //Temperature event
        if (event < 20) {
            TemperatureSensor.temperature = r.nextInt(60);
            System.out.println("The temperature is: "+TemperatureSensor.temperature);
            if (TemperatureSensor.temperature < 25) {
                TemperatureSensor.getHeats().tooCold();
            }
            if (TemperatureSensor.temperature > 40) {
                TemperatureSensor.getCooling().tooHot();
            }
            if(TemperatureSensor.temperature>=25 && TemperatureSensor.temperature<=40){
                System.out.println("The temperature is optimal!");
            }

            System.out.println("\r\n");

        }

        if (event >= 20 && event <= 40) {

            int location = r.nextInt(3);
            fireList.get(location).SetSmoke(r.nextBoolean());

            if (fireList.get(location).GetSmoke() == true) {
                System.out.println("Smoke detected " + fireList.get(location).GetId());
                fireList.get(location).GetAlarms().alarmFire();
                fireList.get(location).GetGsm().callOwner();
            } else {
                System.out.println("There is no smoke!");

            }
            System.out.println("\r\n");


        }
        if (event > 40) {
            System.out.println("all good");
            System.out.println("\r\n");
        }


    }

}
