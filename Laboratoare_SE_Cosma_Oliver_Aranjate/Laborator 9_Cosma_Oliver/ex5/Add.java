package Lab09_ex05;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

   // Used for GUI ADD Train
public class Add extends JFrame {
    static int i=1;
    JTextField station;
    JLabel s;
    JTextField destination;
    JLabel d;
    JTextField segment;
    JLabel se;
    JButton submit;
    ArrayList<Controler> lc;
    //Constructor for the new frame
    Add(ArrayList<Controler> list_controlers)
    {
        lc=list_controlers;
        setTitle("Simulator");
        init();
        setBounds(720,0,300,300);
        setResizable(false);
        setVisible(true);
    }
    //Design for the new frame
    public void init()
    {
        this.setLayout(null);
        int width = 120;
        int height = 20;

        s=new JLabel("Station");
        s.setBounds(50,50,width,height);

        station=new JTextField();
        station.setBounds(120,50,width,height);


        d=new JLabel("Destination");
        d.setBounds(50,80,width,height);
        destination=new JTextField();
        destination.setBounds(120,80,width,height);

        se=new JLabel("Segment");
        se.setBounds(50,110,width,height);
        segment=new JTextField();
        segment.setBounds(120,110,width,height);

        submit=new JButton("Add");
        submit.setBounds(50,140,width,height);
        submit.addActionListener(new SubmitData());

        Random rand = new Random();

        int redValue = rand.nextInt(255);
        int greenValue = rand.nextInt(255);
        int blueValue = rand.nextInt(255);

        Color clr = new Color(redValue, greenValue, blueValue);

        submit.setBackground(clr);

        add(s);
        add(d);
        add(se);
        add(station);
        add(destination);
        add(segment);
        add(submit);
    }


    class SubmitData implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //Take the value from the textBoxes
            String station_string = station.getText();
            String destination_string = destination.getText();
            String segment_string = segment.getText();

            //Variables to check if every data is inserted and valid
            boolean s=false;
            boolean d=false;
            boolean sg=false;
            for(Controler c:lc)
            {
                if(c.stationName.equals(station_string))
                    s=true;
                if(c.stationName.equals(destination_string))
                    d=true;
                //If segment available
                if(c.getFreeSegmentId()!=-1)
                {
                    //Check if the segment exists
                    if(Integer.valueOf(segment_string)>=0 && Integer.valueOf(segment_string)<=2)
                        sg=true;
                }

            }
            //If data is inserted correct and valid
            if(s && d)
            {
                if(sg)
                {
                    //Create a new train instance
                    Train t1=new Train(destination_string,"Add"+i);
                    i++;
                    //Look in the list find the start station
                    for(Controler c:lc)
                    {
                        if(c.stationName.equals(station_string))
                        {
                            //Add a train on the corresponding segment from the destination section
                            c.list.get(Integer.valueOf(segment_string)).arriveTrain(t1);
                        }
                    }
                }
            }

        }
    }
}
