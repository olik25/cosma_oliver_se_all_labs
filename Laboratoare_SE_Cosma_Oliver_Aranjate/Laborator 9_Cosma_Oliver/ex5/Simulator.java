package Lab09_ex05;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class Simulator extends JFrame {
    static int i=0;
    int steps=3;
    //Create an array of JtextFields in order to be displayed on the frame afterwards
    ArrayList<JTextField> stationlist =new ArrayList<JTextField>();
    static ArrayList<Controler> l_controlers=new ArrayList<Controler>();
    JButton start_button;
    JButton add_button;
    //New frame constructor which get the controllers interconnected in main method
    Simulator(ArrayList<Controler> list_controlers)
    {
        l_controlers=list_controlers;
        setTitle("Simulator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init(list_controlers);
        setSize(700,300);
        setVisible(true);
        setResizable(false);
    }

    public void start()
    {
        System.out.println("\nStart train control\n");
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            new Simulator(l_controlers);
            for(Controler cc:l_controlers)
            {
                cc.controlStep();
            }

            System.out.println();

            for(Controler cc:l_controlers)
            {
                cc.displayStationState();
            }
        }
    }
    //Frame design
    public void init(ArrayList<Controler> list_controlers) {
        this.setLayout(null);
        int width = 120;
        int height = 20;
        int i=0;
        int base_x=50;
        int x=50;
        int y=20;

        // Set the station names textFields (first column)
        for(Controler c:list_controlers)
        {
            JTextField aux=new JTextField();
            aux.setBounds(x,y,width,height);
            aux.setEditable(false);
            aux.setText(c.stationName);
            add(aux);

            for(Segment s:c.list){
                // Generates 3 new columns (every segment from any station)
                JTextField aux1=new JTextField();
                aux1.setBounds(x+width+10,y,width,height);
                aux1.setEditable(false);
                // If a segment has a train on it writes it's name and id in the corresponding textField
                if(s.hasTrain())
                    aux1.setText("ID="+String.valueOf(s.id)+"_Train="+s.getTrain().name);
                else
                    aux1.setText("");
                //Adds the segment corresponding textField + set the x position for the next one
                add(aux1);
                x+=130;
            }
            // When a station situation is ready move to the next row(station)
            stationlist.add(aux);
            x=base_x;
            y+=30;
            i++;
        }
        //Creating the two buttons and their corresponding events ( start method/new add frame)
        start_button = new JButton("Start Simulator");
        start_button.setBounds(50, 200, 120, 30);

        start_button.addActionListener(new StartSim());

        add_button = new JButton("Add train");
        add_button.setBounds(190, 200, 120, 30);
        add_button.addActionListener(new AddTrain());

        Random rand = new Random();

        int redValue = rand.nextInt(255);
        int greenValue = rand.nextInt(255);
        int blueValue = rand.nextInt(255);

        Color clr = new Color(redValue, greenValue, blueValue);

        start_button.setBackground(clr);

        redValue = rand.nextInt(255);
        greenValue = rand.nextInt(255);
        blueValue = rand.nextInt(255);

        clr = new Color(redValue, greenValue, blueValue);

        add_button.setBackground(clr);

        add(start_button);
        add(add_button);
    }
    class StartSim implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            start();
        }
    }
    class AddTrain implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            new Add(l_controlers);
        }
    }

    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Brasov
        Controler c2 = new Controler("Brasov");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build station Bucuresti
        Controler c3 = new Controler("Bucuresti");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);

        //testing

        Train t1 = new Train("Brasov", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Bucuresti","R-002");
        s2.arriveTrain(t2);

        Train t3 = new Train("Brasov","IR-012");
        s8.arriveTrain(t3);

        Train t4 = new Train("Cluj-Napoca","IR-020");
        s7.arriveTrain(t4);

        ArrayList<Controler> list_controlers = new ArrayList<Controler>();
        list_controlers.add(c1);
        list_controlers.add(c2);
        list_controlers.add(c3);
        //list_controlers.add(new Controler("da"));

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        new Simulator(list_controlers);
    }

}

class Controler{

    String stationName;

    ArrayList<Controler> neighbourController=new ArrayList<Controler>();

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v){
        neighbourController.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();
                for(Controler controler:neighbourController)
                {
                    if(t.getDestination().equals(controler.stationName)){
                        //check if there is a free segment
                        int id = controler.getFreeSegmentId();
                        if(id==-1){
                            System.out.println("Trenul "+t.name+" din gara "+stationName+" nu poate fi trimis catre "+controler.stationName+". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+controler.stationName);
                        segment.departTRain();
                        controler.arriveTrain(t,id);
                    }
                }
            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public void displayStationState(){
        System.out.println("=== STATION "+stationName+" ===");
        for(Segment s:list){
            if(s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
            else
                System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
        }
    }
}


class Segment{
    int id;
    Train train;

    Segment(int id){
        this.id = id;
    }

    boolean hasTrain(){
        return train!=null;
    }

    Train departTRain(){
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t){
        train = t;
    }

    Train getTrain(){
        return train;
    }
}

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}