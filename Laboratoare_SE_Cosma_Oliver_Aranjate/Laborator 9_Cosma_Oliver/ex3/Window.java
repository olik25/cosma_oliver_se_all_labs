package Lab09_ex03;

import Lab09_ex02.Win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.channels.FileLockInterruptionException;
import java.util.Scanner;

public class Window extends JFrame {
    //Components
    Container c;
    JButton butt;
    JTextArea display;
    JTextField choose;
    JButton buttcur;
    //Constructor
    public Window(String name) {
        super(name);
        init();
        this.setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init() {
        //Background and design
        this.setBounds(300, 200, 1000, 1000);
        this.setBackground(Color.GREEN);
        this.c=getContentPane();
        this.c.setBackground(Color.ORANGE);
        this.c.setLayout(null);
        ///BUTTON///
        butt = new JButton("Path -> File");
        butt.setBounds(770,500,150,150);
        butt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                display.setText("");
                String filename = choose.getText();
                try {
                    BufferedReader in = new BufferedReader(new FileReader(filename));
                    String line;
                    while((line = in.readLine())!= null){
                        display.append(line);
                    }
                    choose.setText("");
                    in.close();
                }
                catch (IOException e) {choose.setText(e.getMessage());}

            }
        });
        c.add(butt);

        ///TEXTFIELD///
        choose = new JTextField("Path/Name(if in current folder)");
        choose.setBounds(530,350,400,60);
        choose.setForeground(Color.DARK_GRAY);
        choose.setBackground(Color.WHITE);
        c.add(choose);
        ///TEXTAREA///
        display = new JTextArea();
        display.setBounds(0,0,500,900);
        display.setBackground(Color.WHITE);
        display.setForeground(Color.RED);
        display.setFont(new Font("Serif", Font.ITALIC, 16));
        display.setLineWrap(true);
        display.setWrapStyleWord(true);
        c.add(display);

        ///BUTTON CURRENT FOLDER///
        buttcur = new JButton("Current folder file");
        buttcur.setBounds(770,100,150,150);
        buttcur.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                display.setText("");
                String filename = choose.getText();
                try {
                    BufferedReader in = new BufferedReader(new FileReader("src/Lab09_ex03/"+filename));
                    String line;
                    while((line = in.readLine())!= null){
                        display.append(line);

                    }
                    choose.setText("");
                    in.close();
                }
                catch (IOException e) {choose.setText(e.getMessage());}

            }
        });
        c.add(buttcur);
    }
}
