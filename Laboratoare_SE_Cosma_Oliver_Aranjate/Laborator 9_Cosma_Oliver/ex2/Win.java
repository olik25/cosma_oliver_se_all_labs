package Lab09_ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Win extends JFrame {
    //Attributes (Window components)
     Container c;
     JButton but;
     TextField textf;
    //The window construction
    public Win(String name) {

        //The global variable which stores how many times the button was clicked
        super(name);
        init();
        //Set the Window visible on the screen and set it's Default Close Operation
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);


    }
    public void init()
    {
        ///WINDOW + CONTAINER///
        this.setBounds(300, 200, 500, 500);
        //Colors the header
        this.setBackground(Color.CYAN);
        //Colors the background
        this.c = getContentPane();
        this.c.setLayout(null);
        this.c.setBackground(Color.GREEN);

        ///BUTTON///
        //Adds a button and an action to it by using an functional interface
        but = new JButton("Click!");
        but.setBounds(350,350,100,100);
        but.addActionListener(new ClickHandeler());
        but.setForeground(Color.RED);
        but.setOpaque(true);
        //We add the button to the container
        c.add(but);

        ///TEXTFIELD///
        //Create a text field and add the result + make it non-editable
        textf = new TextField();
        textf.setBounds(100,100,60,60);
        textf.setText("0");
        textf.setEditable(false); //Not able to edit
        textf.setBackground(Color.RED);//Background color
        textf.setForeground(Color.WHITE);//Text color
        c.add(textf);

    }


    class ClickHandeler implements ActionListener{
        //Attributes
        int nrClicks= 0;
        //Inherited method from ActionListener class
        public void actionPerformed(ActionEvent e){
            nrClicks++;
            Win.this.textf.setText(Integer.toString(nrClicks));
        }
    }



}