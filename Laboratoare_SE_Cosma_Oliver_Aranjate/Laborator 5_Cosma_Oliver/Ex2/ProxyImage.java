package Ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName, char x){
        this.fileName = fileName;
        if(x=='D'){display();}
        if(x=='R'){RotatedImage();}
        if(x!='D' && x!='R'){System.out.println("WRONG OPERATION!");}
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
    public void RotatedImage()
    {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.RotatedImage();
    }
}