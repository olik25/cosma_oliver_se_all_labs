package Ex4;

public abstract class SensorS {

    private String location;
    public abstract int readValue();

    public String getLocation()
    {
        return this.location;
    }
}
