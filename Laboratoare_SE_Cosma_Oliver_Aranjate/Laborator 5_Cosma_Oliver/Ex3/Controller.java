package Ex3;

public class Controller {

    LightSensor ls= new LightSensor();
    TemperatureSensor ts = new TemperatureSensor();
// intarzierea pt sensori din cerinta
    public static void wait(int ms){
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    } public void control()
    {
        for(int i=0;i<20;i++)
        {
            wait(1000);
            System.out.println("Light Sensor: "+ls.readValue()+"  Temperature Sensor:"+ts.readValue());

        }
    }
}
