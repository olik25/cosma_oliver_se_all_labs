package ex6;

public class Chronometer extends Thread {
    Buffer bf;

    Chronometer(Buffer bf){
        this.bf = bf;
    }

    public void run() {
        while (true) {
            bf.incrementTime();
            System.out.println(bf.time);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
