package ex6;

import org.w3c.dom.events.EventException;

public class Buffer {
    int time;

    public synchronized void incrementTime(){
        time++;
    }

    public synchronized void pauseTimer(){
        try{
            wait();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public synchronized void resumeTimer(){
        notify();
    }

    public synchronized void resetTimer(){
        time = 0;
    }

}
