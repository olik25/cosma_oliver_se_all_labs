package ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChronometerUI extends JFrame implements ActionListener {
    Chronometer ch;
    Buffer bf;
    boolean start = false;   // true = start ;;;;;; false = pause

    public ChronometerUI(Chronometer ch, Buffer bf) {
        super("Chronometer");

        this.bf = bf;
        this.ch = ch;
        this.ch.start();
        JButton btnStart = new JButton("Start/Pause");
        JButton btnReset = new JButton("Reset");

        btnStart.addActionListener(this::actionPerformed);
        btnReset.addActionListener(this::actionPerformed);

        this.add(btnStart);
        this.add(btnReset);

        this.setLayout(new FlowLayout());
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBounds(50,50,500,200);
    }

    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        if (action.equals("Start/Pause")) {
            if (start) {
                System.out.println("Start");
                bf.resumeTimer();
            } else {
                System.out.println("Pause");
                bf.pauseTimer();
            }
            start = !start;
        } else if (action.equals("Reset")) {
            bf.resetTimer();
            start = true;
        }
    }
}