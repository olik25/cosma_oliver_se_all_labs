package ex6;

public class Main {
    public static void main(String[] args) {
        Buffer bf = new Buffer();
        Chronometer ch = new Chronometer(bf);
        new ChronometerUI(ch, bf);

    }
}
