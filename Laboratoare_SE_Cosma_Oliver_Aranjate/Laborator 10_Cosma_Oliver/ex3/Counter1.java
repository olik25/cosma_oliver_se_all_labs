package ex3;

public class Counter1 extends Thread {
    Time time;

    Counter1(String name, Time time) {
        super(name);
        this.time = time;
    }

    public void run() {
        synchronized (time) {
            for (time.time = 0; time.time < 20; time.time++) {
                System.out.println(getName() + " i = " + time.time);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " FINISHED");
        }
    }
}
