package ex3;

public class Counter2 extends Thread {
    Time time;

    Counter2(String name, Time time) {
        super(name);
        this.time = time;
    }

    public void run() {
        synchronized (time) {
            if (time.time == 20) {
                for (time.time = 20; time.time < 50; time.time++) {
                    System.out.println(getName() + " i = " + time.time);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println(getName() + " FINISHED");
        }
    }
}
