package ex3;

public class Main {
    public static void main(String[] args) {
        Time time = new Time(0);
        Counter1 C1 = new Counter1("Counter1", time);
        Counter2 C2 = new Counter2("Counter2", time);

        C1.start();
        C2.start();
        System.out.println(time);
    }
}

class Time{
    int time;

    Time(int time){
        this.time = time;
    }
    public void setTime(int time){
        this.time = time;
    }
    public int getTime(){
        return time;
    }
}