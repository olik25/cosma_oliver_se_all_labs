package ex5;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args){
        Buffer b = new Buffer();
        Producer pro = new Producer(b);
        Consumer c = new Consumer(b);
        Consumer c2 = new Consumer(b);
        //Lanseaza cele 3 fire de executie. Se observa ca cele 3 fire de executie

        pro.start();
        c.start();
        c2.start();
    }
}

/**
 * Aceasta este o clasa de tip fir de executie.
 *
 */
class Producer implements Runnable
{
    private Buffer bf;
    private Thread thread;
    Producer(Buffer bf){this.bf=bf;}

    public void start()
    {
        if (thread==null)
        {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void run()
    {
        while (true)
        {
            bf.push(Math.random());
            System.out.println("Am scris.");
            try
            {Thread.sleep(1000);}catch(Exception e){}
        }
    }
}

/**
 * Aceasta este o clasa de tip fir de executie.
 */

class Consumer extends Thread
{
    private Buffer bf;
    Consumer(Buffer bf){this.bf=bf;}

    public void run()
    {
        while (true)
        {
            System.out.println("Am citit : "+this+" >> "+bf.get());
        }
    }
}

class Buffer
{
    /*
     * Vector folosit pentru a inmagazina obiecte de tip Double.
     */
    ArrayList content = new ArrayList();

    /**
     * Prin intermediul acestei metode sunt adaugate elemente in containerul content.
     *
     */
    synchronized void push(double d)
    {
        content.add(new Double(d));
        notify();
    }

    /**
     * Aceasta metoda este folosita pentru a extrage elemente din cadrul containerului
     * content.
     *
     * @return
     */
    synchronized double get()
    {
        double d=-1;
        try
        {
            while(content.size()==0) wait();
            d = (((Double)content.get(0))).doubleValue();
            content.remove(0);
        }catch(Exception e){e.printStackTrace();}
        return d;
    }
}