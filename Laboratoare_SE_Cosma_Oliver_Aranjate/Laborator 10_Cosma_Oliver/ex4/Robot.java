package ex4;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class Robot extends Thread {
    Point position = new Point();
    String name;
    ArrayList<Robot> robots = new ArrayList<Robot>();
    boolean alive = true;

    public Robot(String name, Point position) {
        this.name = name;
        this.position = position;
    }

    public Robot(String name, int x, int y) {
        this.name = name;
        this.position.x = x;
        this.position.y = y;
    }

    public void serRobotList(ArrayList<Robot> robots) {
        this.robots = robots;
    }


    public void run() {
        while (this.alive) {

            Random r1 = new Random();
            Random r2 = new Random();
            int posX = position.getX();
            int posY = position.getY();
            int newX = posX + r1.nextInt(20) - 10;
            int newY = posY + r2.nextInt(20) - 10;
            if ((newX < 200 && newX > 0) && (newY < 200 && newY > 0)) {
                this.position.setX(newX);
                this.position.setY(newY);
                System.out.println(this.name + " At: " + this.position.getX() + " " + this.position.getY());
                checkRobotCollision(this.robots);
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
        }
    }

    public Point getRobotPosition() {
        return position;
    }

    public void destroyRobot() {
        System.out.println("Robot " + this.name + " destroyed at pos: " + this.position.getX() + " " + this.position.getY());
        this.alive = false;
    }

    public void checkRobotCollision(ArrayList<Robot> robots) {
        Point pos1, pos2;
        pos1 = this.getRobotPosition();
        for (Robot r2 : robots) {
            if (!this.name.equals(r2.name)) {
                pos2 = r2.getRobotPosition();
                if (pos1.getX() == pos2.getX() && pos1.getY() == pos2.getY()) {
                    this.destroyRobot();
                }
            }
        }
    }
}