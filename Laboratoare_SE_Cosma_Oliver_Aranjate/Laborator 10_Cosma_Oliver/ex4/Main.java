package ex4;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random r = new Random();
        Robot r1 = new Robot("r1", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r2 = new Robot("r2", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r3 = new Robot("r3", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r4 = new Robot("r4", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r5 = new Robot("r5", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r6 = new Robot("r6", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r7 = new Robot("r7", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r8 = new Robot("r8", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r9 = new Robot("r9", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r10 = new Robot("r10", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r11 = new Robot("r11", r.nextInt(198)+1, r.nextInt(198)+1);
        Robot r12 = new Robot("r12", r.nextInt(198)+1, r.nextInt(198)+1);


        ArrayList<Robot> robots = new ArrayList<Robot>();
        robots.add(r1);
        robots.add(r2);
        robots.add(r3);
        robots.add(r4);
        robots.add(r5);
        robots.add(r6);
        robots.add(r7);
        robots.add(r8);
        robots.add(r9);
        robots.add(r10);
        robots.add(r11);
        robots.add(r12);
        for (Robot robot : robots) {
            robot.serRobotList(robots);
            robot.start();
        }
    }
}

