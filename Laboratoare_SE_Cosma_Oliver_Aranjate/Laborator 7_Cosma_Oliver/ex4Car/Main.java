package Laboratory7.ex4Car;

import java.io.*;
import java.util.Scanner;
import java.io.IOException;

public class Main {


    //We use an ObjectOutputStream that it will write byte to byte the whole object so
    // it needs a basic OutputStream as parameter (location)
    public static void SaveCar(Car car) throws IOException {
        try {
            FileOutputStream fileout = new FileOutputStream("src/Laboratory7/ex4Car/" + car.getModel() + ".ser");
            ObjectOutputStream stdout = new ObjectOutputStream(fileout);
            stdout.writeObject(car);
            System.out.println("Car saved!");
            stdout.close();
        } catch (IOException e) {
            System.out.println("Car was not saved. \n");
        }
    }

    //The function used to deserialize a car
    public static void ReadCar(String str) {
        Car car = new Car();
        try {
            FileInputStream filein = new FileInputStream("src/Laboratory7/ex4Car/" + str + ".ser");
            ObjectInputStream stdin = new ObjectInputStream(filein);
            car = (Car) stdin.readObject();
        } catch (IOException e) {
            System.out.println("Object not found!\r\n");
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("The class of the object not found!\r\n");
            return;
        }
        System.out.println("The Car read :");
        System.out.print("Model: " + car.getModel() + "  Price: " + car.getPrice());
    }

    public static void ViewCars() {
        File f = new File("src/Laboratory7/ex4Car/");
        //A special interface used in filtering files

        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File FILE, String name) {
                return name.endsWith("ser");
            }
        };
        //Create an array where we store all the files remained after the filter application
        File[] files = f.listFiles(filter);
        System.out.println("The current serialized objects are:");
        for (File file : files) {
            System.out.println(file.getName());
        }
    }
//main with commands for console and io exception
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int o = 1;
        while (o != 0)
        {
            System.out.println("GIVE COMMAND:");
            System.out.println("\n1.SAVE A CAR");
            System.out.println("2.READ A CAR");
            System.out.println("3.SEE ALL THE CARS");
            System.out.println("0.EXIT");
            o = in.nextInt();
            switch (o) {
                case 1: {
                    double price;
                    String model;
                    System.out.print("Give the modeL :");
                    model = in.next();
                    System.out.print("Give the price :");
                    price = in.nextDouble();
                    SaveCar(new Car(model, price));
                    break;
                }
                case 2: {
                    String object;
                    System.out.print("What model?");
                    object = in.next();
                    ReadCar(object);
                    break;
                }
                case 3: {
                    ViewCars();
                    break;
                }
                case 0: {
                    break;
                }
                default: {
                    System.out.print("not valid!");
                    break;
                }


            }

        }
    }
}


