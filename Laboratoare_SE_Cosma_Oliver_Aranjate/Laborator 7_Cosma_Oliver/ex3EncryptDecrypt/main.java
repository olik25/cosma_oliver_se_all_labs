package Laboratory7.ex3EncryptDecrypt;

import java.io.*;
import java.util.Scanner;
import java.util.zip.*;


public class main {
    //2 methods
    //Encrypted
    static void encryptFile(String fisierenc) {
        //input buffer reading file
        BufferedReader input = new BufferedReader(new FileReader(fisierenc));
        String newname = "";
        int i = 0;
        while (fisierenc.charAt(i) != '.') {
            newname = newname + fisierenc.charAt(i);
            i++;
        }
        newname = newname + ".enc";

        //we create a new file
        File file = new File("src/Laboratory7/ex3EncryptDecrypt/" + newname);
        // we write the characters in another file
        BufferedWriter output = new BufferedWriter(new FileWriter(newname));
        int character;
        while ((character = input.read()) != -1) {
            character = character << 1;
            output.write(character);
        }
        //Close files
        input.close();
        output.close();

    }

}

    //Decrypted
    static void decryptFile(String name) {

        try {

            BufferedReader input = new BufferedReader(new FileReader(name));
            String newname = "";
            int i = 0;
            while (name.charAt(i) != '.') {
                newname = newname + name.charAt(i);
                i++;
            }
            newname = newname + ".dec";
            File file = new File("src/Laboratory7/ex3EncryptDecrypt/" + newname);
            BufferedWriter output = new BufferedWriter(new FileWriter(newname));
            int character;
            while ((character = input.read()) != -1) {
                character = character >> 1;
                output.write((char) character);
            }
            input.close();
            output.close();


        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }

    //main
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int op = 1;
        while (op != 0) {
            System.out.println("Choose command:");
            System.out.println("1.Encrypt");
            System.out.println("2.Decrypt");
            System.out.println("0.Close");

            op = in.nextInt();
            switch (op) {
                case 1: {
                    System.out.print("\nGive the file to encrypt:");
                    String fisier = in.next();
                    encryptFile("src/Laboratory7/ex3EncryptDecrypt/" + fisier);
                    break;
                }
                case 2: {
                    System.out.print("\nGive the file to decrypt:");
                    String fisier = in.next();
                    decryptFile("src/Laboratory7/ex3EncryptDecrypt/" + fisier);
                    break;
                }
                case 0: {
                    break;
                }
                default: {
                    System.out.println("Invalid command!");
                }
            }

        }

    }

}
