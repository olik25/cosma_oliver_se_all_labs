package ex6;
import java.util.Scanner;
public class factorialnerecursiv {
    public static void main(String[] args) {
        long factorial = 1;
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 1; i <= n; i++)
            factorial *= i;
        System.out.println(factorial);
    }
}
