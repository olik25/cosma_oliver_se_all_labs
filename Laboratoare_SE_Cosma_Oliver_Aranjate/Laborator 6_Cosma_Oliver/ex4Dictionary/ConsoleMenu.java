package Laboratory6.ex4Dictionary;

import java.util.Scanner;
import java.util.HashMap;

public class ConsoleMenu {
    public static void main(String[] args) {
        char selection;
        Dictionary D = new Dictionary();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose an option");
        System.out.println("-------------------------\n");
        System.out.println("1. Add word");
        System.out.println("2. Definition for that word");
        System.out.println("3. All words");
        System.out.println("4.All definitions");
        System.out.println("x - Exit");
        selection = scanner.next().charAt(0);
        switch (selection) {
            case '1':
                System.out.println("Please enter a word and its definition\n");
                Word w = new Word(scanner.nextLine());
                Definition d = new Definition(scanner.nextLine());
                D.addWord(w, d);
                System.out.println("Word was entered successfully!");
                break;
            case '2':
                System.out.println("Please enter a word to find its definition\n");
                Word w1 = new Word(scanner.nextLine());
                D.getDefinition(w1);
                break;
            case '3':
                D.getAllWords();
                break;
            case '4':
                D.getAllDefinitions();
                break;


        }
    }
}
