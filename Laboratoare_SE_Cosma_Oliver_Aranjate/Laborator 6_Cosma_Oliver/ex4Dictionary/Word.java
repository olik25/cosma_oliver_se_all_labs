package Laboratory6.ex4Dictionary;

public class Word {
    private String word;

    public Word() {

    }

    //getters and setters
    public Word(String s) {
        this.word = s;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return this.word;
    }
}
