package Laboratory6.ex4Dictionary;

import java.util.HashMap;

public class Dictionary {

    Word w = new Word();
    Definition d = new Definition();

    public Dictionary() {

    }
//according to diagram from lab
    HashMap<String, String> dictionary = new HashMap<String, String>();

    public void addWord(Word w, Definition d) {
        dictionary.put(w.getWord(), d.getDefiniton());
    }

    public void getDefinition(Word w) {
        System.out.println(dictionary.get(w.getWord()));
    }

    public void getAllWords() {
        for (String i : dictionary.keySet())
            System.out.println(i);
    }

    public void getAllDefinitions() {
        for (String i : dictionary.values())
            System.out.println(i);
    }

}


