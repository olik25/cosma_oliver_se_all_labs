package Laboratory6.ex2Bank;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collector;

public class Bank {

    List<BankAccount> accounts = new ArrayList<BankAccount>();

    public void addAccount(String owner, double balance) {
        BankAccount x = new BankAccount(owner, balance);
        accounts.add(x);
    }


    public void printAccounts() {
        Comparator<BankAccount> comparebybalance = Comparator.comparing(BankAccount::getbalance);
        List<BankAccount> sorted = accounts.stream().sorted(comparebybalance).collect(Collectors.toList());
        System.out.println(sorted);
    }


    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount ba : accounts) {
            if (ba.getbalance() > minBalance && ba.getbalance() < maxBalance) {
                System.out.println(ba.toString());
            }
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount ba : accounts) {
            if (ba.getOwner() == owner) return ba;

        }
        return new BankAccount("NU EXISTA CONT", 0);
    }

    public void getAllAccounts()
    {
        Comparator<BankAccount> comparebyname = Comparator.comparing(BankAccount::getOwner);
        List<BankAccount> sortedn = accounts.stream().sorted(comparebyname).collect(Collectors.toList());
        System.out.println(sortedn);

    }

}

