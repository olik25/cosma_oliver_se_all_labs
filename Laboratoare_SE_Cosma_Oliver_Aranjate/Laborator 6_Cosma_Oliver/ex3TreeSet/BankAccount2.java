package Laboratory6.ex3TreeSet;

import java.util.Objects;

public class BankAccount2 implements Comparable<BankAccount2> {
    static int n;
    private String owner;
    private double balance;

    public BankAccount2(){}
    public BankAccount2(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw (double amount)
    {
        this.balance=this.balance-amount;
    }

    public void deposit (double amount)
    {
        this.balance=this.balance+amount;
    }
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String c)
    {
        this.owner=c;
    }
    public double getbalance()
    {
        return this.balance;
    }
    public void setbalance(double d)
    {
        this.balance= d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o==null||getClass()!=o.getClass()) return false;
        BankAccount2 bankAccount2=(BankAccount2) o;
        return balance==((BankAccount2) o).balance;
    }
    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(balance);

    }

    @Override
    public int compareTo(BankAccount2 bk2)
    {
        return (int)(this.getbalance()-bk2.getbalance());
    }

}