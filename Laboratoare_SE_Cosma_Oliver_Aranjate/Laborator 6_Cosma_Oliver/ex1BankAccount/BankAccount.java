package Laboratory6.ex1BankAccount;

import java.util.Objects;

public class BankAccount {

    private String owner;
    private double balance;

   public BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw (double amount)
    {
        this.balance=this.balance-amount;
    }

    public void deposit (double amount)
    {
        this.balance=this.balance+amount;
    }
    public String getOwner()
    {
        return this.owner;
    }
    public double getbalance()
    {
        return this.balance;
    }
// suprascriere - vezi model de la lab.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(this.balance, balance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
