package Ex1Lab3Cosma;

public class Robot {
    int x;

    public Robot() {
        x = 1;
    }

    void change(int k) {
        while (k >= 1) {
            x = x + k;//smth does not makes sense in the request for me
            k--;
        }
    }

    void toString1() {
        System.out.println(x);
    }

    public static void main(String[] args) {
        Robot r1 = new Robot();
        System.out.println("Initial position of Robot");
        r1.toString1();
        r1.change(3);
        System.out.println("Position after change");
        r1.toString1();
    }
}
