package random;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class random {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("•Minuni și false minuni - moaste, fenomenul de la Maglavit");
        list.add("•Împărtășania - pericol epidemiologic sau nu?");
        list.add("•Covid 19 - stare de alertă vs. stare de urgență");
        list.add("•Cunoastere științifică și cunoaștere duhovnicească");
        list.add("•Banii și BOR");
        list.add("•Relația Biserică – Stat");
        list.add("•Pro-choice si îmbătrânirea populației");
        list.add("•Face sens să avem viață spirituală?");
        list.add("•Poliamoria ");
        random obj = new random();
        // take a random element from list and print them
        System.out.println(obj.getRandomElement(list));
    }

    public String getRandomElement(List<String> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }
}
