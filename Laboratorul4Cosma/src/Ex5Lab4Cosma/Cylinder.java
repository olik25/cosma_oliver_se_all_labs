package Ex5Lab4Cosma;


public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius1) {
        super.radius = radius1;
    }

    public Cylinder(double radius1, double height1) {
        super.radius = radius1;
        height = height1;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea() * height;
    }
}
