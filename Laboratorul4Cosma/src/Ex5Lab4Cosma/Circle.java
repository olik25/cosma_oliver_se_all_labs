package Ex5Lab4Cosma;
import Ex3_4Lab4Cosma.*;
import Ex1Lab4Cosma.*;
import Ex2Lab4Cosma.*;

import java.util.Scanner;

public class Circle {
    protected double radius;
    private String color = "red";

    public Circle() {
    }

    public Circle(double newradius) {
        radius = newradius;
    }

    public double getRadius() {
        int r;
        Scanner in = new Scanner(System.in);
        r = in.nextInt();
        return r;
    }

    public double getArea() {
        int k;
        Scanner in = new Scanner(System.in);
        k = in.nextInt();
        return k;
    }
}