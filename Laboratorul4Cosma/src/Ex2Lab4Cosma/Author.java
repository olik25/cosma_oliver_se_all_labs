package Ex2Lab4Cosma;

public class Author {

    private static String name;
    private static String email;
    private char gender;

    public Author(String name1, String email1, char gender1) {
        name = name1;
        email = email1;
        gender = gender1;
    }

    public static String getName() {
        return name;
    }

    public static String getEmail() {

        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public String toString() {
        System.out.print("email:");
        System.out.print(name);
        System.out.print("(");
        System.out.print(gender);
        System.out.print(")@");
        System.out.print(email);
        System.out.print(".com");
        return null;
    }
}
