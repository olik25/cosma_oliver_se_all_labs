package Ex2Lab4Cosma;

public class Book {
    private int numberAuthors = 5;
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock;

    public Book(String name1, Author[] author1, double price1) {
        name = name1;
        author = author1;
        price = price1;
    }

    public Book(String name1, Author[] author1, double price1, int qtyInStock1) {
        name = name1;
        author = author1;
        price = price1;
        qtyInStock = qtyInStock1;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price1) {
        price = price1;
    }

    public void setQtyInStock(int qtyInStock1) {
        qtyInStock = qtyInStock1;
    }

    public String toString() {
        for (int i = 1; i <= numberAuthors; i++) {
            System.out.print("book:");
            System.out.print(name);
            System.out.print("by author: ");
            System.out.print(author[i].getName());
            System.out.print(" at email:");
            System.out.print(author[i].getEmail());
        }
        return null;
    }

    public void PrintAuthors() {
        for (int j = 1; j <= numberAuthors; j++) {
            System.out.println("Author: ");
            System.out.println(author[j].getName());
            System.out.println(";");
        }


    }
}


