package Ex3_4Lab4Cosma;

import Ex2Lab4Cosma.Author;
import Ex3_4Lab4Cosma.*;
import Ex1Lab4Cosma.*;
import Ex5Lab4Cosma.*;

public class Book {
    private String name;
    private Ex2Lab4Cosma.Author[] author;
    private double price;
    private int qtyInStock;

    public Book(String name1, Ex2Lab4Cosma.Author[] author1, double price1) {
        name = name1;
        author = author1;
        price = price1;
    }

    public Book(String name1, Ex2Lab4Cosma.Author[] author1, double price1, int qtyInStock1) {
        name = name1;
        author = author1;
        price = price1;
        qtyInStock = qtyInStock1;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price1) {
        price = price1;
    }

    public void setQtyInStock(int qtyInStock1) {
        qtyInStock = qtyInStock1;
    }

    public String toString() {
        System.out.print("book:");
        System.out.print(name);
        System.out.print("by author: ");
        System.out.print(Author.getName());
        System.out.print(" at email:");
        System.out.print(Author.getEmail());
        return null;
    }

    public void printAuthors() {
        Author[] aut = new Author[5];
        for (int i = 0; i < 5; i++) {
            System.out.println("name:");
            System.out.println(Author[i].getName());//???
        }

    }
}
