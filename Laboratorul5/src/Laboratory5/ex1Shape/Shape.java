package Laboratory5.ex1Shape;
public abstract class Shape {

    protected String color;
    protected boolean filled;

    public abstract double getArea();
    public abstract double  getPerimeter();
    public abstract String toString();
    public Shape()
    {
        this.color="red";
        this.filled=true;
    }
    public Shape(String color, boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }

    public String getColor()
    {
        return this.color;
    }
    public void setColor(String color)
    {
        this.color=color;
    }
    public boolean isFilled() { return this.filled; }
    public void setFilled(boolean filled)
    {
        this.filled=filled;
    }
}
