package Laboratory5.ex1Shape;

public class main {
    public static void main(String[] args) {
        Circle c=new Circle(3,"yellow",false);
        System.out.println(c.toString());
        System.out.println("The perimeter is: "+c.getPerimeter());
        System.out.println("The area is: "+c.getArea());

        Rectangle r=new Rectangle(4,5,"blue",true);
        System.out.println(r.toString());
        System.out.println("The perimeter is: "+r.getPerimeter());
        System.out.println("The area is: "+r.getArea());


        Square sq=new Square(3,"pink",false);
        System.out.println(sq.toString());
        System.out.println("The perimeter is: "+sq.getPerimeter());
        System.out.println("The area is: "+sq.getArea());



    }
}
