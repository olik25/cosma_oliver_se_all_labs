package Laboratory5.ex1Shape;

public class Circle extends Shape {
    static double pi=3.14159;
    protected double radius;

    public Circle()
    {
        this.radius=1.0;
    }
    public Circle(double radius)
    {
        this.radius=radius;
    }
    public Circle(double radius, String color, boolean filled)
    {
        super(color, filled);
        this.radius=radius;
    }


    public double getRadius()
    {
        return this.radius;
    }
    public void setRadius(double radius)
    {
        this.radius=radius;
    }
    public double getArea()
    {
        return this.radius*this.radius*pi;
    }
    public double getPerimeter()
    {
        return this.radius*2*pi;
    }

    public String toString()
    {
        return "A circle with radius= "+this.radius +" with color of " +super.color + " and filled(" +super.filled+")" ;
    }
}
