package Ex3;

import java.util.Random;

public class LightSensor extends Sensor {

    public int readValue() {
        return new Random().nextInt(100);
    }
}
