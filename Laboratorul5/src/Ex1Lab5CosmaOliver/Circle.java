package Ex1Lab5CosmaOliver;

import java.util.Scanner;

public class Circle extends Shape {
    protected double radius;
    private String color = "red";

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
       return radius;
    }
    public Circle(double radius, String color, boolean filled)
    {
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }
    public double getArea() {
        int k;
        Scanner in = new Scanner(System.in);
        k = in.nextInt();
        return k;
    }
    public double getPerimeter(){
        double perim=2*3.14*radius;
        return perim;
    }
    public String toString(){
        System.out.println(color);
        System.out.println(radius);
        System.out.println(filled);
        return null;
    }
}