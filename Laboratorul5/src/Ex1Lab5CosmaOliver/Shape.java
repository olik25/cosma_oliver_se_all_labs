package Ex1Lab5CosmaOliver;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
        System.out.println("shape created");
    }

    public Shape(String color, boolean filled) {
    }

    ;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract public double getArea();

    abstract public double getPerimeter();

    public String toString() {
        System.out.println("color=");
        System.out.println(color);
        System.out.println("filled is ");
        System.out.println(filled);
        return null;
    }

}
