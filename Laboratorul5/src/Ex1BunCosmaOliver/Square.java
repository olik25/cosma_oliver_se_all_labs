package Laboratory5.ex1Shape;
public class Square extends Rectangle {

    public Square(){};
    public Square(double side)
    {
        super(side,side);
    }
    public Square(double side, String color, boolean filled)
    {
        super(side,side,color,filled);
    }

    public double getSide()
    {
        return super.getWidth();
    }
    public void setSide(double side)
    {
        super.setLength(side);
        super.setWidth(side);
    }
    public void setWidth(double side)
    {
        super.setLength(side);
        super.setWidth(side);
    }
    public void setLenght(double side)
    {
        super.setLength(side);
        super.setWidth(side);
    }

    public String toString()
    {
        return "A square with side "+this.getSide()+" with color of " +super.color + " and filled(" +super.filled+")" ;
    }




}
