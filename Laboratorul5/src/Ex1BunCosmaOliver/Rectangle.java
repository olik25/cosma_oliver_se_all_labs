package Laboratory5.ex1Shape;
public class Rectangle extends Shape {

    protected double width;
    protected double lenght;

    public Rectangle()
    {
        this.lenght=1.0;
        this.width=1.0;
    }
    public Rectangle(double width, double lenght)
    {
        this.width=width;
        this.lenght=lenght;
    }
    public Rectangle(double width, double lenght, String color, boolean filled)
    {
        super(color,filled);
        this.width=width;
        this.lenght=lenght;
    }

    public double getWidth()
    {
        return this.width;
    }
    public void setWidth(double width)
    {
        this.width=width;
    }
    public double getLength()
    {
        return this.lenght;
    }
    public void setLength(double lenght)
    {
        this.lenght=lenght;
    }
    public double getArea()
    {
        return this.lenght*this.width;
    }
    public double getPerimeter()
    {
        return 2*(this.width+this.lenght);
    }

    public String toString()
    {
        return "A Rectangle with width= "+this.width+" and length= "+this.lenght+" with color of " +super.color + " and filled(" +super.filled+")" ;
    }


}