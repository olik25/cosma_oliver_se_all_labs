package Ex4;

import Ex3.Controller;
import Ex3.LightSensor;
import Ex3.TemperatureSensor;

public class ControllerS {
//private static volatile singleton demo instance=null - din laborator
    private static volatile ControllerS control = null;

    ControllerS() { }

    LightSensorS ls = new LightSensorS();
    TemperatureSensorS ts = new TemperatureSensorS();

    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void prcontrol() {
        for (int i = 0; i < 20; i++) {
            wait(1000);
            System.out.println("Light Sensor: " + ls.readValue() + "  Temperature Sensor:" + ts.readValue());

        }
    }
//singleton folosind metoda 2- thread safe- with volatile before
    public static ControllerS getControllerS() {
        synchronized (ControllerS.class) {
            if (control == null) {
                control = new ControllerS();
            }
        }
        return control;
    }
}


