package Laboratory6.ex2Bank;

import java.util.Objects;

public class BankAccount {
    static int n;
    private String owner;
    private double balance;

    public BankAccount(){}
    public BankAccount(String owner, double balance)
    {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw (double amount)
    {
        this.balance=this.balance-amount;
    }

    public void deposit (double amount)
    {
        this.balance=this.balance+amount;
    }
    public String getOwner()
    {
        return this.owner;
    }
    public void setOwner(String c)
    {
        this.owner=c;
    }
    public double getbalance()
    {
        return this.balance;
    }
    public void setbalance(double d)
    {
        this.balance= d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
       BankAccount that = (BankAccount) o;
        return Double.compare(this.balance, balance) == 0;
    }
    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);

    }

}
