package Laboratory6.ex3TreeSet;

import Laboratory6.ex2Bank.BankAccount;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.*;
import java.util.stream.Collectors;

public class Bank2 {

    SortedSet<BankAccount2> accounts = new TreeSet<BankAccount2>();

    public void Account(String owner, double balance) {
        BankAccount2 x = new BankAccount2(owner, balance);
        accounts.add(x);
    }


    public void printAccounts() {
        System.out.println(accounts);
    }


    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount2 ba : accounts) {
            if (ba.getbalance() > minBalance && ba.getbalance() < maxBalance) {
                System.out.println(ba.toString());
            }
        }
    }

    public BankAccount2 getAccount(String owner) {
        for (BankAccount2 ba : accounts) {
            if (ba.getOwner() == owner) return ba;

        }
        return new BankAccount2("NU EXISTA CONT", 0);
    }




}
