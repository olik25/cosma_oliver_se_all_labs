package Laboratory6.ex4Dictionary;

public class Definition {
    private String definition;

    public Definition() {
    }

    //getters and setters
    public Definition(String Definition) {
        this.definition = Definition;
    }

    public void setDefinition(String Definition) {
        this.definition = Definition;
    }

    public String getDefiniton() {
        return this.definition;
    }
}
