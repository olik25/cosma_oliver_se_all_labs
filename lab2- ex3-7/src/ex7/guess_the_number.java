package ex7;

import java.util.Random;
import java.util.Scanner;

public class guess_the_number {
    public static void main(String[] args) {
        Random rand = new Random();
        int x;
        x = rand.nextInt(15);
        int i = 3;
        Scanner input = new Scanner(System.in);
        do {
            int y = input.nextInt();
            if (x == y) {
                System.out.println("Correct!");
                break;
            } else if (x > y) {
                System.out.println("The number you chose is smaller");
            } else if (x < y) System.out.println("The number you chose is greater");
            i--;
        }
        while (i != 0);
        if (i == 0) {
            System.out.println("Game over");
            System.out.println("The number was:" + x);
        }
    }
}
