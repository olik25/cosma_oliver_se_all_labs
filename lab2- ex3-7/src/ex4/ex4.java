package ex4;
import java.util.Scanner;
public class ex4 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] numbers = new int[n];
        numbers[0] = in.nextInt();
        int max = numbers[0];
        for (int i = 1; i < n; i++) {
            numbers[i] = in.nextInt();
            if (numbers[i] > max) max = numbers[i];
        }
        System.out.println(max);
    }
}
