package ex6;

import java.util.Scanner;

public class factorialrecursiv {

    static long factorial(int n) {
        if (n <= 2) return n;
        else return (n * factorial(n - 1));
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        long x = factorial(n);
        System.out.println(x);

    }

}
