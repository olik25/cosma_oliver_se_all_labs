package Ex1CosmaOliverLab6;

public class BankAccount {
    private String owner;
    private double balance;



    public void deposit(double amount) {
        this.balance=amount;
    }
    public void withdraw(double amount) {
        this.balance=this.balance-amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount x = (BankAccount)obj;
            return (x.balance == balance);
        }
        return false;

    }

    public int hashCode() {
        return (int) balance;
    }
}