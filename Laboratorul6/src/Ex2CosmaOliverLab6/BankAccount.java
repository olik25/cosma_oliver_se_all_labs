package Ex2CosmaOliverLab6;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;
    public BankAccount(){}
    public BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public String getName() {
        return owner;
    }

    public void setOwner(String newName) {
        this.owner = newName;
    }

    public double getBalance(){
        return balance;
    }

    public void setBalance(double newBalance){
        this.balance=newBalance;
    }

    //for accounts
    public void deposit(double amount) {
        this.balance = this.balance+ amount;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }
    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);

    }

}
