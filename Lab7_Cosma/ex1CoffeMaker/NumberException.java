package Laboratory7.ex1CoffeMaker;

public class NumberException extends Exception {
    int n;
    public NumberException(int n,String msg) {
        super(msg);
        this.n = n;
    }
    int getNr()
    {
        return n;
    }
}
