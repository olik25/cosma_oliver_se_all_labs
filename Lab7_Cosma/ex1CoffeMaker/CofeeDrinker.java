package Laboratory7.ex1CoffeMaker;

public class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException,NumberException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        if(c.getNr()==10)
            throw new NumberException(c.getNr(),"You made 10 coffees!");
        System.out.println("Drink cofee:"+c);
    }
}
