package Laboratory7.ex1CoffeMaker;

public class CoffeTest
{
    public static void main(String[] args) {
    CofeeMaker mk = new CofeeMaker();
    CofeeDrinker d = new CofeeDrinker();
    int n=0;
    for(int i = 0;i<15;i++){
        n++;
        Cofee c = mk.makeCofee(n);
        try {
            d.drinkCofee(c);
        } catch (TemperatureException e) {
            System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
        } catch (ConcentrationException e) {
            System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
        }
        catch(NumberException e)
        {
            System.out.println("Exception:"+e.getMessage()+" nr="+e.getNr());
        }
        finally{
            System.out.println("Throw the cofee cup.\n");
        }
    }
}
}
