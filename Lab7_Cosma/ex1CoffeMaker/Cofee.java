package Laboratory7.ex1CoffeMaker;

public class Cofee{
    private int temp;
    private int conc;
    private int nr;

    Cofee(int t,int c,int n){temp = t;conc = c;nr=n;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getNr(){return nr;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"cofee number="+nr+"]";}
}
