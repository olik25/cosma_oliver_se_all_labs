package Laboratory7.ex2Character;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.io.IOException;

public class main {
    public static void main(String[] args) throws IOException {
        Scanner filein = new Scanner(System.in);
        //exception for file
        try {
            //reading input by lines-from lab
            BufferedReader txt = new BufferedReader(new FileReader("src/Laboratory7/ex2Character/data.txt"));
            // we read the text as integers
            int no = 0;
            int c, ch;

            System.out.println("Give the character for which you want to find the frequency: ");
            ch = filein.next().charAt(0);
            //input from memory
            while ((c = txt.read()) != -1) {
                System.out.print((char) c);
                if (ch == c) no++;

            }
            System.out.println("\nThere are " + no + " " + (char) ch);
            txt.close();
        } catch (FileNotFoundException e) {
            System.err.println("I/O error:" + e.getMessage());

        }

    }
}
