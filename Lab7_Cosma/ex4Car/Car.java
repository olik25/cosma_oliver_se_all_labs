package Laboratory7.ex4Car;

import java.io.Serializable;

public class Car implements Serializable {
    //attributes
    private String model;
    private double price;

    //constructors
    public Car(String model , double price)
    {
        this.model=model;
        this.price=price;}
    public Car(){}
    //Getters and setters
    public void setModel(String mod)
    {
        this.model=mod;
    }
    public void setPrice(double pri)
    {
        this.price=pri;
    }
    public String getModel()
    {
        return this.model;
    }
    public double getPrice()
    {
        return this.price;
    }


}
